﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USBWebServices.IntegrationTest.TestCases
{
    internal static class TestData
    {
        public static class Gender
        {
            public static readonly Guid Female = new Guid("2FF8E63B-0A36-E511-80C8-005056B8008E");

            public static readonly Guid Male = new Guid("31F8E63B-0A36-E511-80C8-005056B8008E");
        }
        public static class Title
        {
            public static readonly Guid Adjudant_General = new Guid("96C1855B-0736-E511-80C8-005056B8008E");
        }

        public static class Ethnicity
        {
            public static readonly Guid African = new Guid("A061F10C-0C36-E511-80C8-005056B8008E");
        }

        public static class Nationality
        {
            public static readonly Guid Antigua = new Guid("3B89E0E1-0736-E511-80C8-005056B8008E");
        }
        public static class ShortCourseOffering
        {
            public static readonly Guid CourseX = new Guid("F46F79D2-0F36-E511-80C8-005056B8008E");
            public static readonly Guid CourseY = new Guid("a8133738-4286-e611-80d9-005056b8008e");
            
        }

        public static class CourseOffering
        {
            public static readonly Guid FundamentalsOfRiskManagement = new Guid("32D49693-D14F-E611-80D6-005056B8603D");
        }
       





    }
}
