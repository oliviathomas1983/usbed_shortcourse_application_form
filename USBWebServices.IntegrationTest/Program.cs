﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USBWebServices.IntegrationTest.TestCases;

namespace USBWebServices.IntegrationTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;          

          Test_CreateContactAndEnrollStudent();
          // Test_DocumentUpload();
            //Test_GetStudentNumber();

            Console.Read();
        }

        static void Test_GetStudentNumber()
        {
            ExecuteTest("GetStudentNumber", () =>
            {
                var client = new USBWebServiceRef.StudentServiceClient();

                var result = client.GetStudentNumber("Upload@Testing2.comm");
                client.Close();
            });
        }

        static void Test_CreateContactAndEnrollStudent()
        {
            ExecuteTest("CreateContactAndEnrollStudent", () =>
            {
                var client = new USBWebServiceRef.StudentServiceClient();
               

                var testEmail = $"{Guid.NewGuid()}@eoh.com";

            var result = client.CreateContactAndEnrollStudent(
                TestData.ShortCourseOffering.CourseX,
                "persone@Testing2.com",
                "Testing2",
                "Testing2",
                "Testing2",
                "MRG",
                TestData.Title.Adjudant_General,
                TestData.Gender.Male,
                TestData.Nationality.Antigua,
                TestData.Ethnicity.African,
                DateTime.Now.AddYears(-25),
                "12345678912354",                   
                new Guid("8812171E-0A36-E511-80C8-005056B8008E"),//FOreign
                //new Guid("8A12171E-0A36-E511-80C8-005056B8008E"),//ID
                 null,
                 null,
                 null,
                 // "3301015800088",
                 null,
                 new Guid("64681D1A-744A-E511-80CB-005056B8008E"),
                 null,
                 new Guid("2BB7599A-0736-E511-80C8-005056B8008E"),
                 new Guid("BBFF68A9-0736-E511-80C8-005056B8008E"),
                 false,
                 "QA",
                 new Guid("FF24B8F9-0736-E511-80C8-005056B8008E"),
                 new Guid("5ED6D57A-0B36-E511-80C8-005056B8008E"),
                     //   new Guid("EA02D25F-0A36-E511-80C8-005056B8008E"), /// province, ///suburb, city, postal code for International
                     "Vredehoek",
                     "Cape Town",
                     "Unit 409 Canterbury Square", //street 1
                     "32 Glynn Street", //street 2
                     "8001",
                     //null,
                     //"kr",
                     //"kr",
                     //null,
                     //null,
                     //"8000",
                     "0321312321840871377"
                     );


                client.Close();
            });
        }

        static void Test_DocumentUpload()
        {
            ExecuteTest("DocumentUpload", () =>
            {
                var testPdfPath = Path.Combine(Environment.CurrentDirectory, "TestCases", "test.pdf");
                var client = new USBWebServiceRef.StudentServiceClient();

              

                using (var fs = File.OpenRead(testPdfPath))
                {
                    try
                    {
                        var fileLength = Convert.ToInt32(fs.Length);
                        var fileData = new byte[fileLength];
                        fs.Read(fileData, 0, fileLength);
                   
                        client.UploadRequiredDocuments(TestData.ShortCourseOffering.CourseX, "Upload@Testing2.com", fileData, "ID", "test.pdf", "Identity Documentation" );

                    }
                    finally
                    {
                        fs.Close();
                        client.Close();
                    }
                }

            });
        }

        static void ExecuteTest(string testDesc, Action test)
        {
            Console.WriteLine("-----------------------------------------------------------");
            Console.WriteLine(DateTime.Now.AddDays(+1));
            Console.WriteLine($"Test {testDesc} starting ...");

            try
            {
                test();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex);

                Console.ForegroundColor = ConsoleColor.Green;
            }
            Console.WriteLine(DateTime.Now.AddDays(+1));
            Console.WriteLine($"Test {testDesc} completed.");
        }

    }
}
