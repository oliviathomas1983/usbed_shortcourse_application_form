﻿namespace USB_ED.Utilities.WCF
{
    public class EndpointConfiguration
    {
        public EndpointConfiguration(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}