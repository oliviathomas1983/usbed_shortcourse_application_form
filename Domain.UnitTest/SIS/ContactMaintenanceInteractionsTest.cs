﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;

namespace Domain.UnitTest
{
    [TestClass]
    public class ContactMaintenanceInteractionsTest
    {
        [TestMethod]
        public void GetUSNumber()
        {
            // Arrange
            var person = new Person
            {
                FirstNames = "Danie",
                SouthAfricanIdNumber = "8906215047085",
                Surname = "Strydom",
                Initials = "D",
                DateOfBirth = new DateTime(1989, 6, 21),
                Title = "10",
                Gender = "M",
                Ethnicity = "1",
                Nationality = "100",
                CorrespondenceLanguage = "1",
                Language = "13",
                EmailAddress = "danie@d.com"
            };

            // Act
            var reply = person.CreateApplicant(person);

            // Assert
            Assert.IsTrue(reply.Success && reply.Message.IsNullOrEmpty() && reply.ExistedAlready && reply.UsNumber.IsNotNullOrEmpty());
        }
    }
}
