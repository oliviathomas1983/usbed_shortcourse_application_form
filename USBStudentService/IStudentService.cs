﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace USBStudentService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IStudentService
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        string GetStudentNumber(string email);

        [OperationContract]
        CreateContactAndEnrollStudentResult CreateContactAndEnrollStudent(Guid ShortCourseOffering, string EmailAdress, string LastName, string FirstNames,
          string NameOnCertificate, string Initials, Guid Title, Guid Gender, Guid Nationality, Guid Ethnicity, DateTime DateOfBirth,
          string ForeignIdNumber, Guid ForeignIdType, DateTime? ForeignIdExpiryDate, string PassportNumber, DateTime? PassportExpiryDate,
          string IdNumber, Guid DietaryRequirements, string DietaryRequirementsOther, Guid Language, Guid Disability, bool DoYouUseAWheelchair,
          string Environment, Guid PostalCountry, Guid? Province, string PostalSuburb,
          string PostalCity, string PostalStreet1, string PostalStreet2, string PostalPostalCode, string CellphoneNumber);

        [OperationContract]
        UploadRequiredDocumentsResult UploadRequiredDocuments(Guid ShortCourseOffering, string EmailAdress, byte[] documentBytes, string type, string documentName, string documentDescription);


        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class UploadRequiredDocumentsResult
    {
        bool success = true;
        string usNumber = "";
        string message = "";
        Guid enrollmentId = Guid.Empty;


        [DataMember]
        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        [DataMember]
        public Guid EnrolmentId
        {
            get { return enrollmentId; }
            set { enrollmentId = value; }
        }

        [DataMember]
        public string UsNumber
        {
            get { return usNumber; }
            set { usNumber = value; }
        }

        [DataMember]
        public string Message
        {
            get { return message; }
            set { message = value; }
        }
    }

    [DataContract]
    public class CreateContactAndEnrollStudentResult
    {
        bool success = true;
        string usNumber = "";
        string message = "";
        Guid enrollmentId = Guid.Empty;


        [DataMember]
        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        [DataMember]
        public Guid EnrolmentId
        {
            get { return enrollmentId; }
            set { enrollmentId = value; }
        }

        [DataMember]
        public string UsNumber
        {
            get { return usNumber; }
            set { usNumber = value; }
        }

        [DataMember]
        public string Message
        {
            get { return message; }
            set { message = value; }
        }
    }
}
