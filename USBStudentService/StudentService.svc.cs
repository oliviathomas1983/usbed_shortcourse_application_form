﻿using ShortCourseApplicationForm.Domain.CRM;
using ShortCourseApplicationForm.Domain.Models.Enums;
using ShortCourseApplicationForm.Domain.Controller;
using System;
using System.Collections.Generic;
using System.Linq;

namespace USBStudentService
{
   
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class StudentService : IStudentService
    {
        private ShortCourseApplicationFormsApiController apiController = new ShortCourseApplicationFormsApiController();

        public ApplicantStatus ApplicantStatus { get; private set; }

        public string GetStudentNumber(string email)
        {

            string result = null;
            result = CrmQueries.GetUsNumber(email);
            if (result.IsNullOrEmpty())
            { return "Student number not found."; }
            return result;

        }

        public CreateContactAndEnrollStudentResult CreateContactAndEnrollStudent(          
          Guid ShortCourseOffering,
          string EmailAdress,
          string LastName,
          string FirstNames,
          string NameOnCertificate,
          string Initials,
          Guid Title,
          Guid Gender,
          Guid Nationality,
          Guid Ethnicity,
          DateTime DateOfBirth,
          string ForeignIdNumber,
          Guid ForeignIdType,
          DateTime? ForeignIdExpiryDate,
          string PassportNumber,
          DateTime? PassportExpiryDate,
          string IdNumber,
          Guid DietaryRequirements,
          string DietaryRequirementsOther,
          Guid Language,
          Guid Disability,
          bool DoYouUseAWheelchair,
          string Environment,
          Guid PostalCountry,
          Guid? Province,
          string PostalSuburb,
          string PostalCity,
          string PostalStreet1,
          string PostalStreet2,
          string PostalPostalCode,
          string CellphoneNumber
            )
        {

            Guid enrolmentId;
            Guid contactId;
            string UsNumber = CrmQueries.GetUsNumber(EmailAdress);


            string correspondingLanguageCode = CrmQueries.GetSisCorrespondingLanguageCode(Language);

            if (UsNumber == "" || UsNumber == null)
            {
                try
                {
                    UsNumber = ShortCourseApplicationForm.Domain.SIS.ContactMaintenanceInteractions.GetUSNumber(FirstNames, LastName, Initials, EmailAdress, DateOfBirth, Title,
                    Gender, CellphoneNumber, Ethnicity, Nationality, Language, IdNumber, PassportNumber, PassportExpiryDate.GetValueOrDefault(), ForeignIdNumber,
                    correspondingLanguageCode);
                }
                catch (ApplicationException ae)
                {
                    return new CreateContactAndEnrollStudentResult { Success = false, Message = ae.Message };
                }
                catch (Exception ex)
                {
                    return new CreateContactAndEnrollStudentResult
                    {
                        Success = false,
                        Message = "Could not get US Number for applicant. " + ex.Message
                    };
                }
            }

            try
            {
                enrolmentId = CrmQueries.GetEnrolmentId(UsNumber, ShortCourseOffering);
                contactId = CrmQueries.GetContactId(UsNumber);

                CrmActions.CreateOrUpdateContact(ShortCourseOffering, EmailAdress, LastName, FirstNames,
                               NameOnCertificate, Initials, Title, Gender, Nationality, Ethnicity, DateOfBirth, ForeignIdNumber,
                               ForeignIdType, ForeignIdExpiryDate, PassportNumber, PassportExpiryDate, IdNumber, DietaryRequirements,
                               DietaryRequirementsOther, Language, Disability, DoYouUseAWheelchair, contactId, enrolmentId, UsNumber, Environment,
                               PostalCountry, Province, PostalSuburb, PostalCity, PostalStreet1, PostalStreet2, PostalPostalCode, CellphoneNumber);
            }
            catch (Exception ex)
            {
                return new CreateContactAndEnrollStudentResult
                {
                    Success = false,
                    Message = "Could not create or update contact. " + ex.Message
                };
            }

            return new CreateContactAndEnrollStudentResult
            {
                Success = true,
                Message = "",
                UsNumber = UsNumber,
                EnrolmentId = enrolmentId
            };
        }


        public UploadRequiredDocumentsResult UploadRequiredDocuments(Guid ShortCourseOffering, string EmailAdress, byte[] documentBytes, string type, string documentName,string documentDescription)
        {
            var DocumentLevel = 0;
           
            Guid enrolmentId;

            string UsNumber = CrmQueries.GetUsNumber(EmailAdress);
            enrolmentId = CrmQueries.GetEnrolmentId(UsNumber, ShortCourseOffering);
           
            var shortCourseId = CrmQueries.LoadShortCourseBy(ShortCourseOffering);
            List<ShortCourseApplicationForm.Domain.Models.Document> RequiredDocuments = CrmQueries.GetRequiredDocuments(shortCourseId, true);

            var requiredDocument = RequiredDocuments.FirstOrDefault();
            if(requiredDocument.Description == "ID" || requiredDocument.Description == "Matric")
            {
                DocumentLevel = 1;
            }
            else if (requiredDocument.Description == "Acceptance")
            {
                DocumentLevel = 3;
            }

            try
            {
                apiController.SubmitDocumentation(enrolmentId, UsNumber,
                       documentName, DocumentLevel, documentDescription,
                       requiredDocument.DocumentId, documentBytes);
            }
            catch (Exception ex)
            {
                return new UploadRequiredDocumentsResult
                {
                    Success = false,
                    Message = "Could not Upload Document/s. " + ex.Message
                };
            }

            return new UploadRequiredDocumentsResult
            {
                Success = true,
                Message = "OK",
                
            };
          

            
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

       
    }
}
