﻿using System;
using System.Collections.Generic;
using ShortCourseApplicationForm.Domain.CRM;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using ShortCourseApplicationForm.Models;

namespace ShortCourseApplicationForm.Domain.Controller
{
    public partial class ShortCourseApplicationFormsApiController
    {
        public void DietaryRequirements(ApplicationFormInfo info)
        {
            Dropdowns.DietaryRequirements(info);
        }

        public void Ethnicities(ApplicationFormInfo info)
        {
            Dropdowns.Ethnicities(info);
        }

        public void EthnicitiesNonSA(ApplicationFormInfo info)
        {
            Dropdowns.EthnicitiesNonSA(info);
        }

        public void ForeignIdentificationTypes(ApplicationFormInfo info)
        {
            Dropdowns.ForeignIdentificationTypes(info);
        }

        public void Genders(ApplicationFormInfo info)
        {
            Dropdowns.Genders(info);
        }

        public void Industries(ApplicationFormInfo info)
        {
            Dropdowns.Industries(info);
        }

        public void Languages(ApplicationFormInfo info)
        {
            Dropdowns.Languages(info);
        }

        public void MarketingReasons(ApplicationFormInfo info)
        {
            Dropdowns.MarketingReasons(info);
        }

        public void MarketingSources(ApplicationFormInfo info)
        {
            Dropdowns.MarketingSources(info);
        }

        public void Nationalities(ApplicationFormInfo info)
        {
            Dropdowns.Nationalities(info);
        }

        public void OccupationalCategories(ApplicationFormInfo info)
        {
            Dropdowns.OccupationalCategories(info);
        }

        public void PaymentResponsibilities(ApplicationFormInfo info)
        {
            Dropdowns.PaymentResponsibilities(info);
        }

        public void QualificationFields(ApplicationFormInfo info)
        {
            Dropdowns.QualificationFields(info);
        }

        public void QualificationTypes(ApplicationFormInfo info)
        {
            Dropdowns.QualificationTypes(info);
        }

        public void Titles(ApplicationFormInfo info)
        {
            Dropdowns.Titles(info);
        }

        public void WorkAreas(ApplicationFormInfo info)
        {
            Dropdowns.WorkAreas(info);
        }

        public void AddressCountries(ApplicationFormInfo info)
        {
            Dropdowns.AddressCountries(info);
        }

        public void Provinces(ApplicationFormInfo info)
        {
            Dropdowns.Provinces(info);
        }

        public void Disabilities(ApplicationFormInfo info)
        {
            Dropdowns.Disabilities(info);
        }

        public void YesNo(ApplicationFormInfo info)
        {
            Dropdowns.YesNo(info);
        }

        public void Years(ApplicationFormInfo info)
        {
            Dropdowns.Years(info);
        }
    }
}
