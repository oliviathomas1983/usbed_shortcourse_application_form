﻿using System;
using ShortCourseApplicationForm.Domain.Crm;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using ShortCourseApplicationForm.Domain.Sharepoint;

namespace ShortCourseApplicationForm.Domain.Controller
{
    public partial class ShortCourseApplicationFormsApiController
    {
        public ApplicationSubmissionResult SubmitDocumentation(Guid enrollment, string studentNumber,
            string documentName, int documentLevel, string documentDescription, int documentId, byte[] documentBytes)
        {
            try
            {
                if (!CrmFileUpload.DoesDocumentLocationForEnrollmentExist(enrollment))
                {
                    CrmFileUpload.CreateUploadedDocumentLocation(enrollment);
                }

                // Add document
                SharepointActions.UploadDocument(documentName, studentNumber, documentLevel, "",
                    documentDescription, documentId, enrollment, documentBytes);

                return new ApplicationSubmissionResult {Successful = true, Message = "OK" };
            }
            catch (Exception ex)
            {
                return new ApplicationSubmissionResult {Successful = false, Message = ex.Message};
            }
        }
    }
}
