﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ShortCourseApplicationForm.Domain.CRM;
using ShortCourseApplicationForm.Domain.Models;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using ShortCourseApplicationForm.Domain.Sharepoint;
using ShortCourseApplicationForm.Models;

namespace ShortCourseApplicationForm.Domain.Controller
{
    public partial class ShortCourseApplicationFormsApiController
    {

        public static string LoadShortCourseOfferingNameByID(Guid ShortCourseOfferingId)
        {
            return CrmQueries.LoadShortCourseOfferingNameByID(ShortCourseOfferingId);

        }
        public static string LoadShortCourseNameByID(Guid ShortCourseId)
        {
            return CrmQueries.LoadShortCourseNameByID(ShortCourseId);

        }

        

        public Guid LoadShortCourseBy(Guid shortCourseOfferingId)
        {
            return CrmQueries.LoadShortCourseBy(shortCourseOfferingId);


        }
        public List<SelectListItem> LoadShortCourseOfferings(ApplicationForm model, string shortCourseId,
            string shortCourseOfferingId)
        {
            return CrmQueries.LoadShortCourseOfferings(model, shortCourseId, shortCourseOfferingId);
        }

        public void SetShortCourseOfferingValues(ApplicationForm model,Guid shortCourseOfferingId)
        {
           CrmQueries.SetShortCourseOfferingValues(model, shortCourseOfferingId);
        }

        

        public Guid GetContactId(string usNumber)
        {
            return CrmQueries.GetContactId(usNumber);
        }

        public Guid GetEnrolmentId(string usNumber, string shortCourseOffering)
        {
            return CrmQueries.GetEnrolmentId(usNumber, shortCourseOffering);
        }

        public ApplicationForm GetContact(ApplicationForm model, string studentNumber)
        {
            return CrmQueries.GetContact(model, studentNumber);
        }

        public List<Document> GetRequiredDocuments(Guid shortCourseId, int level = 1)
        {
            return CrmQueries.GetRequiredDocuments(shortCourseId, level);
        }

        public List<Document> GetUploadedDocuments(Guid enrollment)
        {
            return SharepointActions.GetUploadedDocuments(enrollment);
        }

        public bool HasUploadedDocuments(Guid enrollment)
        {
            return SharepointActions.HasUploadedDocuments(enrollment);
        }

        public bool HasUploadedRequiredDocuments(Guid enrollmentId)
        {
            return SharepointActions.HasUploadedRequiredDocuments(enrollmentId);
        }

        public bool HasUploadedAcceptanceForm(Guid enrollment)
        {
            return SharepointActions.HasUploadedAcceptanceForm(enrollment);
        }

        public bool HasUploadedCVForm(Guid enrollment)
        {
            return SharepointActions.HasUploadedCVForm(enrollment);
        }


        public string GetUsNumber(string email)
        {
            return CrmQueries.GetUsNumber(email);
        }

        public AccountDetails GetAccount(Guid? accountId)
        {
            return CrmQueries.GetAccount(accountId);
        }
        public bool ApplicationClosed(Guid offeringID)
        {
            return CrmQueries.ApplicationClosed(offeringID);
        }
    }
}
