﻿using System;
using ShortCourseApplicationForm.Domain.CRM;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using static System.String;

namespace ShortCourseApplicationForm.Domain.SIS
{
    public static class ContactMaintenanceInteractions
    {
        public static string GetUSNumber(ApplicationForm contactDetails, string sisCorrespondingLanguageCode)
        {
            var applicant = new Person();

            if (IsNullOrEmpty(contactDetails.FirstNames))
            {
                throw new Exception("First names of applicant are missing.");
            }

            if (IsNullOrEmpty(contactDetails.IdNumber) == false && IsNullOrEmpty(contactDetails.PassportNumber) && IsNullOrEmpty(contactDetails.ForeignIdNumber))
            {
                applicant.SouthAfricanIdNumber = contactDetails.IdNumber.Trim();
            }
            else
            {
                applicant.SouthAfricanIdNumber = "NONE";
            }

            applicant.FirstNames = IsNullOrEmpty(contactDetails.GivenName) == false ? contactDetails.GivenName.Trim() : contactDetails.FirstNames.Trim();
            applicant.Surname = contactDetails.Surname.Trim();
            applicant.FirstNames = contactDetails.FirstNames.Trim();
            applicant.Initials = contactDetails.Initials.Trim().ToUpper();
           // applicant.NameCalledBy = contactDetails.NameCalledBy.Trim();
            applicant.EmailAddress = contactDetails.Email.IsNotNullOrEmpty() ? contactDetails.Email.Trim() : "";
            applicant.DateOfBirth = contactDetails.DateOfBirth;
            applicant.Title = CrmIdToSisCodeMapping.GetTitleSisCode(contactDetails.Title);
            applicant.Gender = CrmIdToSisCodeMapping.GetGenderSisCode(contactDetails.Gender);
            applicant.Ethnicity = CrmIdToSisCodeMapping.GetEthnicitySisCode(contactDetails.Ethnicity.GetValueOrDefault());

            if (contactDetails.Nationality != Guid.Empty)
            {
                applicant.Nationality = CrmIdToSisCodeMapping.GetNationalitySisCode(contactDetails.Nationality);
            }

            applicant.CorrespondenceLanguage = sisCorrespondingLanguageCode;

            if (applicant.CellphoneNumber.IsNotNullOrEmpty())
            {
                applicant.CellphoneNumber = contactDetails.CellphoneNumber;
            }
            
            //applicant.GivenName = contactDetails.GivenName;
            applicant.Language = CrmIdToSisCodeMapping.GetLanguageSisCode(contactDetails.Language);
            //applicant.WorkTelephoneNumber = contactDetails.EmployerContactNumber;

            if (IsNullOrEmpty(contactDetails.PassportNumber) == false)
            {
                applicant.PassportNumber = contactDetails.PassportNumber;
                applicant.PassportExpiryDate = contactDetails.PassportExpiryDate;
            }

            if (IsNullOrEmpty(contactDetails.ForeignIdNumber) == false)
            {
                applicant.ForeignIdNumber = contactDetails.ForeignIdNumber;
                applicant.PassportExpiryDate = contactDetails.PassportExpiryDate;
            }

            var reply = applicant.CreateApplicant(applicant);

            if ((reply.Success == false) || (reply.UsNumber == "0"))
            {
                throw new Exception(reply.Message);
            }

            return reply.UsNumber;
        }


        public static string GetUSNumber(string FirstNames,string Surname, string Initials,string Email,DateTime DateOfBirth,Guid Title,
            Guid Gender, string CellphoneNumber,Guid Ethnicity, Guid Nationality,Guid Language, string IdNumber, string PassportNumber,DateTime PassportExpiryDate, string ForeignIdNumber, string sisCorrespondingLanguageCode)
        {
            var applicant = new Person();

            if (IsNullOrEmpty(FirstNames))
            {
                throw new Exception("First names of applicant are missing.");
            }

            if (IsNullOrEmpty(IdNumber) == false && IsNullOrEmpty(PassportNumber) && IsNullOrEmpty(ForeignIdNumber))
            {
                applicant.SouthAfricanIdNumber = IdNumber.Trim();
            }
            else
            {
                applicant.SouthAfricanIdNumber = "NONE";
            }

            applicant.FirstNames = FirstNames.Trim();
            applicant.Surname = Surname.Trim();
            applicant.FirstNames = FirstNames.Trim();
            applicant.Initials = Initials.Trim().ToUpper();
            applicant.EmailAddress =Email.Trim() ;
            applicant.DateOfBirth = DateOfBirth;
            applicant.Title = CrmIdToSisCodeMapping.GetTitleSisCode(Title);
            applicant.Gender = CrmIdToSisCodeMapping.GetGenderSisCode(Gender);
            applicant.Ethnicity = CrmIdToSisCodeMapping.GetEthnicitySisCode(Ethnicity);

            if (Nationality != Guid.Empty)
            {
                applicant.Nationality = CrmIdToSisCodeMapping.GetNationalitySisCode(Nationality);
            }

            applicant.CorrespondenceLanguage = sisCorrespondingLanguageCode;

            if (applicant.CellphoneNumber.IsNotNullOrEmpty())
            {
                applicant.CellphoneNumber = CellphoneNumber;
            }

            //applicant.GivenName = contactDetails.GivenName;
            applicant.Language = CrmIdToSisCodeMapping.GetLanguageSisCode(Language);
            //applicant.WorkTelephoneNumber = contactDetails.EmployerContactNumber;

            if (IsNullOrEmpty(PassportNumber) == false)
            {
                applicant.PassportNumber = PassportNumber;
                applicant.PassportExpiryDate = PassportExpiryDate;
            }

            if (IsNullOrEmpty(ForeignIdNumber) == false)
            {
                applicant.ForeignIdNumber = ForeignIdNumber;
                applicant.PassportExpiryDate = PassportExpiryDate;
            }

            var reply = applicant.CreateApplicant(applicant);

            if ((reply.Success == false) || (reply.UsNumber == "0"))
            {
                throw new Exception(reply.Message);
            }

            return reply.UsNumber;
        }
    }
}
