﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using ShortCourseApplicationForm.Domain.Sharepoint;

namespace ShortCourseApplicationForm.Domain.Tasks
{
    public static class SharepointTask
    {
        public static string UploadFile(string fileName, string documentTypeName, int level,
                                        int documentId, Guid enrollment, 
                                        string studentNumber, byte[] bytes, string reviewed)
        {
            try
            {
                SharepointActions.UploadDocument(fileName, studentNumber, level,  "", documentTypeName, documentId, enrollment, bytes);
            }
            catch(Exception exception)
            {
                return exception.Message;
            }
            return string.Empty;
        }

    }
}