﻿using System.Text;
using System.Web;

namespace Domain.Helpers
{
    public static class UrlSafeEncode
    {
        /// <summary>
        /// Encodes the specified input, removing unsafe web characters
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string Encode(string input)
        {
            byte[] encodingBuffer = Encoding.UTF8.GetBytes(input);
            var encoded = HttpServerUtility.UrlTokenEncode(encodingBuffer);

            return encoded;
        }

        public static string Decode(string input)
        {
            byte[] decodingBuffer = HttpServerUtility.UrlTokenDecode(input);
            return Encoding.UTF8.GetString(decodingBuffer);
        }

    }
}
