﻿using Microsoft.Xrm.Client;
using System.Configuration;
using SettingsMan = ShortCourseApplicationForm.Domain.Properties.Settings;

namespace ShortCourseApplicationForm.Domain.CRM
{
    public static class CrmServer
    {

        private const string key = "CrmConnectionString";

        public static CrmConnection CrmConnection
        {
            get
            {
                var setting = ConfigurationManager.AppSettings[key] ?? SettingsMan.Default.CrmConnectionString;

                return CrmConnection.Parse(setting);
            }
        }
    }
}