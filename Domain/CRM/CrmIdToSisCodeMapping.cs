﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StellenboschUniversity.UsbEd.Integration.Crm;

namespace ShortCourseApplicationForm.Domain.CRM
{
    public static class CrmIdToSisCodeMapping
    {
        public static string GetTitleSisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                stb_title lookup = null;

                try
                {
                    lookup = (from s in crmServiceContext.stb_titleSet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.stb_SisCode;
            }
        }

        public static string GetGenderSisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                stb_gender lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.stb_genderSet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                switch (lookup.stb_NameEnglish.ToUpper())
                {
                    case "MALE":
                        return "M";

                    case "FEMALE":
                        return "V";

                    case "NON BINARY":
                        return "N";

                    default:
                        throw new Exception("No gender names found.");
                }
            }
        }

        public static string GetEthnicitySisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                stb_ethnicity lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.stb_ethnicitySet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.stb_SisCode;
            }
        }

        public static string GetNationalitySisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                stb_nationality lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.stb_nationalitySet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.stb_SisCode;
            }
        }

        public static string GetLanguageSisCode(Guid crmId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                stb_language lookup = null;
                try
                {
                    lookup = (from s in crmServiceContext.stb_languageSet
                              where s.Id == crmId
                              select s).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (lookup == null)
                {
                    throw new Exception();
                }

                return lookup.stb_SisCode;
            }
        }
    }
}
