﻿using System;
using System.Linq;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using ShortCourseApplicationForm.Domain.CRM;
using StellenboschUniversity.UsbEd.Integration.Crm;
using static System.String;
using SettingsMan = ShortCourseApplicationForm.Domain.Properties.Settings;

namespace ShortCourseApplicationForm.Domain.Crm
{
    public static class CrmFileUpload
    {
        public static void CreateUploadedDocumentLocation(Guid enrollmentId)
        {
            CreateDocumentLocationForEnrollment(enrollmentId);
        }

        public static void CreateDocumentLocationForEnrollment(Guid enrollmentId)
        {
            if ( !DoesDocumentLocationForEnrollmentExist(enrollmentId) )
            {
                Guid siteId = GetSharepointSiteId(SettingsMan.Default.CrmSharepointSiteSet);

                using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
                {
                    var sharePointDocumentLocation = new SharePointDocumentLocation
                    {
                        Name = enrollmentId.ToString(),
                        Description = "",
                        ParentSiteOrLocation = new CrmEntityReference(SharePointSite.EntityLogicalName, siteId),
                        RelativeUrl = enrollmentId.ToString(),
                        RegardingObjectId = new CrmEntityReference(stb_enrollment.EntityLogicalName, enrollmentId)
                    };

                    try
                    {
                        organizationService.Create(sharePointDocumentLocation);
                    }
                    finally
                    {
                        organizationService.Dispose();
                    }
                }
            }
        }

        public static bool DoesDocumentLocationForEnrollmentExist(Guid enrollmentId)
        {
            SharePointDocumentLocation crmResult = null;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var result = (from a in crmServiceContext.SharePointDocumentLocationSet
                        select a);

                    foreach (var item in result)
                    {
                        if (!item.RelativeUrl.IsNotNullOrEmpty()) continue;
                        Guid newGuid;

                        var guidItem = Guid.TryParse(item.RelativeUrl, out newGuid);

                        if (newGuid == Guid.Empty) continue;
                        if (Guid.Parse(item.RelativeUrl) != enrollmentId)
                            continue;
                        crmResult = item;
                        break;
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

            }

            if (crmResult == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private static Guid GetSharepointSiteId(string siteName)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                SharePointSite crmResult = null;

                try
                {
                    crmResult = (from q in crmServiceContext.SharePointSiteSet
                                 where q.Name == siteName
                                 select q).First();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmResult == null)
                {
                    throw new Exception();
                }

                return crmResult.Id;
            }
        }

    }
}
