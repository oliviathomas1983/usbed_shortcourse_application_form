﻿
using Microsoft.SharePoint.Client;
using ShortCourseApplicationForm.Domain.Models;
using ShortCourseApplicationForm.Domain.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using SettingsMan = ShortCourseApplicationForm.Domain.Properties.Settings;

namespace ShortCourseApplicationForm.Domain.Sharepoint
{
    public static class SharepointActions
    {
        private static readonly ICredentials SharepointCredentials =
            new NetworkCredential(SettingsMan.Default.SharepointUsername, SettingsMan.Default.SharepointPassword,
                SettingsMan.Default.SharepointDomain);

        private static readonly string SharepointUrl = SettingsMan.Default.SharepointUrl;
        private static readonly string DocumentLibrary = SettingsMan.Default.SharepointLibrary;

        #region Public Methods

        public static void UploadDocument(string documentTitle, string studentNumber, int level, string shortCourse,
            string documentTypeName, int documentId, Guid enrollment, byte[] documentData)
        {
            try
            {
                using (ClientContext sharepointContext = GetClientContext(SharepointUrl, SharepointCredentials))
                {
                    documentTitle = RemoveSpecialCharacters.RemoveInvalidCharactersFromFileName(documentTitle);

                    string folderName = enrollment.ToString();

                    Folder folder = GetEnrolmentFolder(folderName, DocumentLibrary, sharepointContext);

                    DeleteFileFromFolderMatchingMetaTagValue(folder, sharepointContext, "ID", documentId.ToString());

                    File file = CreateFile(folder, documentTitle, documentData, sharepointContext);

                    SetFileMetaData(file, documentTitle, level, documentTypeName, documentId.ToString(),
                        studentNumber, "", sharepointContext);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Document> GetUploadedDocuments(Guid enrollment)
        {
            using (ClientContext sharepointClientContext = GetClientContext(SharepointUrl, SharepointCredentials))
            {
                string folderName = enrollment.ToString();

                Folder folder = GetEnrolmentFolder(folderName, DocumentLibrary, sharepointClientContext);

                if (folder.IsNotNull())
                {
                    return GetAllDocumentsInFolder(folder, sharepointClientContext);
                }

                return null;
            }
        }

        public static bool HasUploadedDocuments(Guid enrollment)
        {
            string folderName = enrollment.ToString();

            Folder folder = GetEnrolmentFolder(folderName, DocumentLibrary,
                GetClientContext(SharepointUrl, SharepointCredentials));

            return (folder.IsNotNull() && folder.ItemCount > 0);
        }

        public static bool HasUploadedRequiredDocuments(Guid enrollmentId)
        {
            var documents = GetUploadedDocuments(enrollmentId);

            return documents.IsNotNull() && documents.Any(x => x.Description == "ID") &&
                   documents.Any(x => x.Description == "Matric Certificate") && documents.Any(x => x.Description == "CV");
        }

        public static bool HasUploadedAcceptanceForm(Guid enrollment)
        {
            var documents = GetUploadedDocuments(enrollment);

            return documents.IsNotNull() && documents.Any(x => x.Description == "Acceptance Letter");
        }
        public static bool HasUploadedCVForm(Guid enrollment)
        {
            var documents = GetUploadedDocuments(enrollment);

            return documents.IsNotNull() && documents.Any(x => x.Description == "CV");
        }




        #endregion

        #region Private Methods

        private static ClientContext GetClientContext(string sharepointUrl, ICredentials credentials)
        {
            ClientContext clientContext = new ClientContext(sharepointUrl)
            {
                AuthenticationMode = ClientAuthenticationMode.Default,
                Credentials = credentials
            };

            return clientContext;
        }

        private static Folder GetEnrolmentFolder(string folderName, string documentLibrary,
            ClientContext clientContext)
        {
            try
            {
                var web = clientContext.Web;

                var list = web.Lists.GetByTitle(documentLibrary);

                var newFolder = list.RootFolder.Folders.Add(folderName);

                if (!newFolder.IsNotNull()) return newFolder;
                clientContext.Load(newFolder);
                clientContext.ExecuteQuery();

                return newFolder;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void DeleteFileFromFolderMatchingMetaTagValue(Folder folder, ClientContext clientContext,
                                                                     string metaTagName, string metaTagValue)
        {
            if (folder.ItemCount > 0)
            {
                var fileCollection = folder.Files;
                clientContext.Load(fileCollection);
                clientContext.ExecuteQuery();

                File fileTarget = null;

                foreach (var file in fileCollection)
                {
                    clientContext.Load(file, f => f, f => f.ListItemAllFields,
                                             f => f.ListItemAllFields[metaTagName]);
                    clientContext.ExecuteQuery();

                    var item = file.ListItemAllFields;

                    if (!string.Equals(metaTagValue, item[metaTagName].AsString(), StringComparison.Ordinal)) continue;
                    fileTarget = file;
                    break;
                }

                fileTarget?.DeleteObject();
            }

            clientContext.ExecuteQuery();
        }

        private static File CreateFile(Folder parentFolder, string fileName, byte[] documentData, ClientContext clientContext)
        {
            FileCreationInformation fileCreationInformation = new FileCreationInformation
            {
                Content = documentData,
                Url = fileName,
                Overwrite = true
            };


            File file = parentFolder.Files.Add(fileCreationInformation);
            clientContext.Load(file);
            clientContext.ExecuteQuery();

            return file;
        }

        private static void SetFileMetaData(File file, string documentTitle, int level,
                                            string documentTypeName, string documentTypeId,
                                            string usNumber, string reviewedYesNo, ClientContext clientContext)
        {
            ListItem item = file.ListItemAllFields;

            item["Title"] = documentTitle;
            //item["Programme"] = programmeName;
            item["DocumentType"] = documentTypeName;
            item["DocumentTypeID"] = documentTypeId;
            item["StudentNo"] = usNumber;
            //item["Reviewed"] = reviewedYesNo;
            item["Level"] = level;

            item.Update();

            clientContext.ExecuteQuery();
        }

        private static List<Document> GetAllDocumentsInFolder(Folder folder, ClientContext clientContext)
        {
            if (folder.ItemCount > 0)
            {
                FileCollection fileCollection = folder.Files;
                clientContext.Load(fileCollection);
                clientContext.ExecuteQuery();

                List<Document> uploadedDocuments = new List<Document>();

                foreach (Microsoft.SharePoint.Client.File file in fileCollection)
                {
                    clientContext.Load(file, f => f, f => f.ListItemAllFields);
                    clientContext.ExecuteQuery();

                    ListItem item = file.ListItemAllFields;

                    uploadedDocuments.Add(new Document()
                    {
                        DocumentId = Convert.ToInt32(item["ID"]),
                        DocumentTypeId = Guid.Empty,//item.FieldValues.ContainsKey("DocumentTypeID") ? Guid.Parse(item["DocumentTypeID"].ToString()) : Guid.Empty,
                        Description = item["DocumentType"].AsString(),
                        FileName = item["Title"].AsString(),
                        UploadedDate = ((DateTime)item["Modified"]).ToString("dd/MM/yyyy"),
                        Level = item["Level"].IsNull() ? 0 : int.Parse(item["Level"].AsString())
                    });
                }

                return uploadedDocuments;
            }

            return null;
        }

        #endregion
    }
}
