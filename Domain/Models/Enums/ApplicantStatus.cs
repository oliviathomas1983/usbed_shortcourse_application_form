﻿namespace ShortCourseApplicationForm.Domain.Models.Enums
{
    public enum ApplicantStatus
    {
        Applied = 1,
        Qualified = 956390017,
        Approved = 956390018
    }
}
