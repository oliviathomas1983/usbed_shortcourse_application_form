﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortCourseApplicationForm.Domain.Models.Enums
{
    public enum DelegateStatus
    {
        Nominated = 1,
        URLSent = 956390000,
        Applied = 956390001
    }
}
