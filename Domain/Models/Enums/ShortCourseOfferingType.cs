﻿namespace ShortCourseApplicationForm.Domain.Models.Enums
{
    public enum ShortCourseOfferingType
    {
        General = 1,
        AdpEdp = 2,
        Gap = 3,
        Public = 4,
        NPO = 5,
        Partnership = 6,
        TEL = 7
    }
}
