﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortCourseApplicationForm.Domain.Models.Enums
{
    public enum VoucherStatus
    {
        Redeemable = 1,
        Redeemed = 956390000,
        Expired = 956390001,
        None = 0
    }
}
