﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ShortCourseApplicationForm.Domain.CRM;
using StellenboschUniversity.UsbEd.Integration.Crm;

namespace ShortCourseApplicationForm.Models
{
    public class AreaList
    {
        public Guid Id { get; set; }
        public string DisplayText { get; set; }
    }


    public class PostalDistributionArea
    {
        #region Properties

        public List<SelectListItem> Areas { get; set; }

        #endregion

        #region Public Methods

        public List<SelectListItem> LoadAreas(string query)
        {
            Areas = new List<SelectListItem>();
            if (!string.IsNullOrEmpty(query))
            {
                using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
                {
                    IQueryable<stb_afrigis> crmResults = null;

                    try
                    {
                        crmResults = from a in crmServiceContext.stb_afrigisSet
                            where a.stb_PostalCode.Contains(query)
                            select a;
                    }
                    finally
                    {
                        crmServiceContext.Dispose();
                    }

                    if (crmResults.IsNotNull())
                    {
                        Areas.Add(new SelectListItem {Text = "Results found. Please select...", Value = "0"});

                        foreach (var item in crmResults)
                        {
                            Areas.Add(new SelectListItem {Text = $"{item.stb_Suburb},{item.stb_City},{item.stb_PostalCode}", Value = item.Id.AsString()});
                        }
                    }
                }
            }

            return Areas;
        }

        #endregion
    }
}