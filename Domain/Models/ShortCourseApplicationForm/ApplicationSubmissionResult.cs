﻿using System;

namespace ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm
{
    public class ApplicationSubmissionResult
    {
        public string USNumber { get; set; }

        public Guid? EnrolmentId { get; set; }

        public decimal? OfferingAmount { get; set; }

        public string OfferingName { get; set; }

        public bool Successful { get; set; }

        public string Message { get; set; }
    }
}
