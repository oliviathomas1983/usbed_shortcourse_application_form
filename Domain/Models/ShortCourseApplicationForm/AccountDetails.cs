﻿using System;
using ShortCourseApplicationForm.Domain.Models.Enums;

namespace ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm
{
    public class AccountDetails
    {
        public Guid? AccountId { get; set; }

        public string AccountNumber { get; set; }

        public string Name { get; set; }

        public AccountType AccountType { get; set; }
    }
}
