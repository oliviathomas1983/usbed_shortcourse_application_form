﻿using System;

namespace ShortCourseApplicationForm.Domain.Models
{
    public class Document
    {
        public Document()
        {
        }

        public Guid? DocumentTypeId { get; set; }

        public bool IsExcluded { get; set; }
        public bool IsDeleted{ get; set; }

        public string DocumentType { get; set; }
       
        public string Description { get; set; }
        public string FileName { get; set; }
        public string UploadedDate { get; set; }
        public string Status { get; set; }
        public bool Reviewed { get; set; }
        public int Level { get; set; }
        public string Data { get; set; }
        public int DocumentId { get; set; }
        public int Size { get; set; }

        public bool ReviewStateChanged { get; set; }

        public bool CanReview { get; set; } = true;
    }
}